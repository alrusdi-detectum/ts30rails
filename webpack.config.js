'use strict'

module.exports = {
  mode: 'production',
  entry: [
    './dist/main.js'
  ],
  resolve: {
    alias: {
        'vue$': 'vue/dist/vue.esm.js'
    }
  }
};
