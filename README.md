# README #

Для разработки требуется npm.

### Сборка и запуск ###

* Установить зависимости командой npm install
* Собрать приложение с помощью npm run build
* Открыть index.html в браузере

### Установка зависимостей ###

* Список зависимостей берется из package.json (раздел dependencies)
* less - собирает css файлы
* typescript - компилирует .ts в js
* vue - фреймворк для построения реактивных UI
* webpack - управление импортами и упаковкой js

### Команды npm ###

* Выполняются вызовом npm run <имя_команды>
* Берутся из блока scripts в package.json
* Помимо системных программ, умеют вызывать программы из node_modules (less, tsc)

### TypeScript ###

* Нужен, чтобы ад и бардак JS превратить в ад статической типизации
* Настраивается в файле tsconfig.json

### VUE ###

* Все что находится внутри тега, выбранного при инициализации Vue через опцию el (el: "#app"), динамически обрабатывается шаблонизатором
* Как только мы меняем переменные в data шаблон перерисовывается с новыми значениями
* Взаимодействия с сервером Vue не предполагает. Это нужно делать самостоятельно и способов много