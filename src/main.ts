import Vue from "vue";

function randomInteger(min:number, max:number): number {
    let rand:number = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}

function rand():number {
    return randomInteger(1, 6)
}

interface Ts30RailsModel {
    left_dice_val: number;
    right_dice_val: number;
    turns_count: number;
    right_dice_rot: number;
    left_dice_rot: number;
}

new Vue({
    el: '#app',
    data: (): Ts30RailsModel  => {
        return {
            "left_dice_val": 1,
            "right_dice_val" : 3,
            "turns_count": 0,
            "right_dice_rot": 0,
            "left_dice_rot": 0
        } as Ts30RailsModel
    },
    methods: {
        roll_left_die: function () {
            if (this.turns_count > 0) return;
            this.left_dice_rot = 1;
            var that = this;
            setTimeout(function() {
                that.left_dice_rot = 0;
                that.left_dice_val = rand();
            }, 500)
        },
        rotate: function () {
            if (this.right_dice_rot == 3) {
                this.right_dice_rot = 0;
            } else {
                this.right_dice_rot ++;
            }
        },
        next_round: function () {
            this.right_dice_rot = 0;
            if (this.turns_count >= 30) {
                alert("Game over!");
                return;
            } 
            this.left_dice_val = rand();
            this.right_dice_val = rand();
            this.turns_count ++;
        }
    }
});